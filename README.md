# Docker Compose for Node.js, Vue, Nginx and PostgreSQL

## Run

    docker-compose up --build

## Test

```
http://localhost:8080/ping
# {"environment":"development","database":"up"}
```
