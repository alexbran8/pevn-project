FROM node:latest
EXPOSE 3000 9229

WORKDIR /home/app/server

COPY server/package.json /home/app/server
COPY server/package-lock.json /home/app/server

RUN npm ci

COPY . /home/app/server

RUN npm install tsc -g
RUN tsc

WORKDIR /home/app/server

CMD ./scripts/start.sh
