import Vue from 'vue';
import VueRouter, { RouteConfig } from "vue-router";
import Home from '@/components/Home.vue';
// import Login from '@/components/Login.vue';
// import Register from '@/components/Register.vue';



export default new VueRouter({
  mode: "history",
  routes: [{
    path: '/',
    name: 'home',
    component:  Home 
  }]
});

