import { Sequelize } from "sequelize";
import { UserFactory, UserStatic } from "./user.model.js";

const dotenv = require('dotenv');

dotenv.config();

export interface DB {
  User: UserStatic;
}



export const dbConfig  = new Sequelize(process.env.DB || '', process.env.DB || '', process.env.DB || '', {
  host: process.env.HOST,
  dialect: "postgres",
  pool: {
    max: 5,
    min: 0,
    acquire: 3000,
    idle: 1000,
  },
});

const User = UserFactory(dbConfig);

export const db: DB = {
    User,
};
