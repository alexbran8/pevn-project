import express from "express";
import { Client } from "pg";
import { healthRoutes } from './routes/health.routes';
import { dbConfig } from "./models/index.js";
import { db } from "./models";

const path = require('path');
const dotenv = require('dotenv');

dotenv.config();

const PORT = process.env.PORT || 3000;

dbConfig
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
  })
  .catch((err) => {
    console.error("Unable to connect to the database:", err);
  });


const app = express();

app.use(express.static(path.join(__dirname, '../../client/dist')));
app.get('/', (req,res) => {
  res.sendFile(path.join(__dirname, '../../client/public/index.html'));
});

app.get("/ping", async (req, res) => {
  const database = await dbConfig.query("SELECT 1 + 1").then(() => "up").catch(() => "down");

  res.send({
    environment: process.env.NODE_ENV,
    database,
  });
});

app.use(healthRoutes);


(async () => {
  app.listen(PORT, () => {
    console.log("Started at http://localhost:%d", PORT);
  });
})();
